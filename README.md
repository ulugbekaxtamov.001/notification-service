# Notification service

### Deploy notification service to k8s

1. Create secret with docker-registry name
   for [pulling image from private registry](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)
2. Change image tag on [deployment.yaml](kubernetes/deployment.yaml) and apply with kubectl

````yaml
kubectl apply -f kubernetes -n <namespace>
````

### Run by Docker-compose

1. Create docker images

````yaml
docker-compose build
````

2. Run docker image using docker compose it runs at port http://127.0.0.1:8000

````yaml
docker-compose up
````

### Дополнительные задание каторые были сделаны

```yaml
  1. организовать тестирование написанного кода
  2. обеспечить автоматическую сборку/тестирование с помощью GitLab CI
  3. подготовить docker-compose для запуска всех сервисов проекта одной командой
  4. написать конфигурационные файлы (deployment, ingress, …) для запуска проекта в 
     kubernetes и описать как их применить к работающему кластеру
  5. сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API. 
     Пример: https://petstore.swagger.io
  6. реализовать администраторский Web UI для управления рассылками и получения статистики по отправленным сообщениям
  
  8. реализовать дополнительный сервис, который раз в сутки отправляет статистику по обработанным рассылкам на email
  
  11. реализовать дополнительную бизнес-логику: добавить в сущность "рассылка" поле "временной интервал", в котором можно задать 
      промежуток времени, в котором клиентам можно отправлять сообщения с учётом их локального времени. Не отправлять клиенту сообщение, 
      если его локальное время не входит в указанный интервал.
      
  12. обеспечить подробное логирование на всех этапах обработки запросов, чтобы при эксплуатации была возможность найти в логах всю информацию по
      • id рассылки - все логи по конкретной рассылке (и запросы на api и внешние запросы на отправку конкретных сообщений)
      • id сообщения - по конкретному сообщению (все запросы и ответы от внешнего сервиса, вся обработка конкретного сообщения)
      • id клиента - любые операции, которые связаны с конкретным клиентом (добавление/редактирование/отправка сообщения/…)
```