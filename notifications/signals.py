import datetime
from .tasks import process_dispatch
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from .models import Dispatch


@receiver(post_save, sender=Dispatch)
def change_format(sender, instance, created, **kwargs):
    if created:
        if instance.start_date_time < datetime.datetime.now():
            process_dispatch(instance.id)
