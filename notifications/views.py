from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from django.views.generic import TemplateView, DetailView, ListView
from .models import Client, Dispatch, Message
import pytz


class IndexView(LoginRequiredMixin, TemplateView):
    template_name = 'index.html'


class ClientListView(LoginRequiredMixin, ListView):
    model = Client
    queryset = Client.objects.all()
    template_name = 'notifications/client_list.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        resp = super(ClientListView, self).get_context_data(**kwargs)
        resp['timezones'] = pytz.all_timezones
        return resp


class DispatchListView(LoginRequiredMixin, ListView):
    model = Dispatch
    queryset = Dispatch.objects.all()
    template_name = 'notifications/dispatch_list.html'


class MessageListView(LoginRequiredMixin, ListView):
    model = Message
    queryset = Message.objects.all()
    template_name = 'notifications/message_list.html'
