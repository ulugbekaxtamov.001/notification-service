from django.core.mail import send_mail
from main.settings import EMAIL_HOST_USER


def send_sms_mail(email="defoult.email1@gmail.com", message="This is a test email sent using Django."):
    try:
        subject = 'Hand Detection'
        message = message
        from_email = EMAIL_HOST_USER
        recipient_list = [email]
        send_mail(subject, message, from_email, recipient_list)
    except:
        pass
