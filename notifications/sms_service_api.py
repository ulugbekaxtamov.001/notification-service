from main.settings import TOKEN_SMS_API, DOMAIN_SMS_API
import requests


def send_sms(phone_number, id_message, message: str = "Message service"):
    data = {
        "id": id_message,
        "phone": phone_number,
        "text": message
    }
    headers = {"Authorization": f"JWT {TOKEN_SMS_API}"}
    resp = requests.post(url=f"{DOMAIN_SMS_API}v1/send/{id_message}", data=data, headers=headers)
    return resp
