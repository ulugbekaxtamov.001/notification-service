from django.db import models
from base.models import Base
from django.core.exceptions import ValidationError


def validate_phone_number_format(value):
    if not value.isdigit() or len(value) != 11 or not value.startswith('7'):
        raise ValidationError('Please enter a valid phone number in the format 7XXXXXXXXXX.')


class Client(Base):
    full_name = models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)

    phone_number = models.CharField(max_length=11, unique=True, validators=[validate_phone_number_format])
    operator_code = models.CharField(max_length=10)
    tag = models.CharField(max_length=255)
    timezone = models.CharField(max_length=255)

    def __str__(self):
        return f"{self.id} | {self.phone_number}"


class Message(Base):
    STATUS_CHOICES = (
        ('sent', 'Sent'),
        ('pending', 'Pending'),
        ('failed', 'Failed'),
    )

    sent_at = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=255, choices=STATUS_CHOICES)
    dispatch = models.ForeignKey('Dispatch', on_delete=models.CASCADE, related_name='messages')
    client = models.ForeignKey(Client, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.id} | {self.client} | {self.status}"


class Dispatch(Base):
    title = models.CharField(max_length=255, null=True, blank=True)
    enabled = models.BooleanField(default=True)

    start_date_time = models.DateTimeField()
    end_date_time = models.DateTimeField()
    message = models.TextField()
    operator_code = models.CharField(max_length=10)
    tag = models.CharField(max_length=255)
