from celery import shared_task
from .models import Client, Dispatch, Message
from django.utils import timezone
from .sms_service_api import send_sms
from .mail_sending import send_sms_mail


@shared_task
def process_dispatch():
    dispatchers = Dispatch.objects.all()
    for dispatch in dispatchers:
        clients = Client.objects.filter(
            operator_code=dispatch.operator_code,
            tag=dispatch.tag
        )
        current_time = timezone.now()

        for client in clients:
            if dispatch.start_time <= current_time <= dispatch.end_time:
                message = Message.objects.create(
                    status='pending',
                    dispatch=dispatch,
                    client=client
                )
                res = send_sms(
                    phone_number=client.phone_number,
                    id_message=message.id,
                    message=dispatch.message
                )
                if res.status_code == 200:
                    message.status = 'sent'
                    message.save()
                else:
                    message.status = 'failed'
                    message.save()


@shared_task
def send_status_mail():
    sent_messages = Message.objects.filter(status='sent').count()
    pending_messages = Message.objects.filter(status='pending').count()
    failed_messages = Message.objects.filter(status='failed').count()
    text = f"sent messages: {sent_messages}\n" \
           f"pending messages: {pending_messages}\n" \
           f"failed messages: {failed_messages}"

    send_sms_mail(message=text)
