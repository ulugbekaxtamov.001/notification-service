from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from .models import Client
from django.urls import reverse


class APITestCase(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_create_client(self):
        url = reverse('api_notification:client_create')
        data = {
            "phone_number": "71234567890",
            "operator_code": "123",
            "tag": "example",
            "timezone": "UTC+3",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201)

        # Проверка, что клиент был создан успешно
        client_id = response.data["id"]
        client = Client.objects.get(id=client_id)
        self.assertEqual(client.phone_number, "71234567890")
        self.assertEqual(client.operator_code, "123")
        self.assertEqual(client.tag, "example")
        self.assertEqual(client.timezone, "UTC+3")
