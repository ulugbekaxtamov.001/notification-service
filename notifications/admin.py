from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from .models import Client, Dispatch, Message


# ********* Default Admin **********************
class DefaultAdmin(SimpleHistoryAdmin):
    exclude = ('author', 'deleted_at')

    actions = ['restore_obj', 'delete_from_database']

    @admin.action(description='Restore Object')
    def restore_obj(self, request, queryset):
        for obj in queryset:
            try:
                obj.restore()
            except:
                pass

    @admin.action(description='Erase from database')
    def delete_from_database(self, request, queryset):
        for obj in queryset:
            try:
                obj.erase()
            except:
                pass


# *********** Tabular Inlines  ******************
class MessageInline(admin.TabularInline):
    model = Message
    extra = 0
    exclude = ['deleted_at', 'updated_at']

    def get_queryset(self, request):
        return Message.all_objects.all()


@admin.register(Client)
class ClientAdmin(DefaultAdmin):
    inlines = [MessageInline]
    exclude = ['deleted_at']
    list_display = ('id', 'phone_number', 'operator_code', 'tag', 'timezone', 'is_delete')
    list_filter = ['is_delete', 'deleted_at', 'operator_code', 'tag']
    search_fields = ['id', 'phone_number', 'operator_code', 'tag']

    def get_queryset(self, request):
        return Client.all_objects.all()


@admin.register(Dispatch)
class DispatchAdmin(DefaultAdmin):
    exclude = ['deleted_at']
    list_display = (
        'id', 'start_date_time', 'end_date_time', 'message',
        'operator_code', 'tag', 'is_delete'
    )
    list_filter = ['is_delete', 'operator_code', 'tag']

    def get_queryset(self, request):
        return Dispatch.all_objects.all()


@admin.register(Message)
class DispatchAdmin(DefaultAdmin):
    exclude = ['deleted_at']
    list_display = (
        'id', 'sent_at', 'status', 'dispatch', 'client', 'is_delete'
    )
    list_filter = ['is_delete', 'status']

    def get_queryset(self, request):
        return Message.all_objects.all()
