from django.urls import path
from . import views

app_name = 'notifications'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('client/list/', views.ClientListView.as_view(), name='client_list'),
    path('dispatch/list/', views.DispatchListView.as_view(), name='dispatch_list'),
    path('message/list/', views.MessageListView.as_view(), name='message_list'),
]
