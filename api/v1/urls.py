from django.urls import path, include

urlpatterns = [
    path('notification/', include('api.v1.notification.urls')),
]
