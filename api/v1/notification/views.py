from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from notifications.models import Client, Dispatch, Message
from .serializers import ClientSerializer, DispatchSerializer, MessageSerializer


# Client
class ClientListApiView(generics.ListAPIView):
    # permission_classes = [IsAuthenticated]

    serializer_class = ClientSerializer
    queryset = Client.objects.all()


class ClientCreateApiView(generics.CreateAPIView):
    # permission_classes = [IsAuthenticated]

    serializer_class = ClientSerializer
    queryset = Client.objects.all()


class ClientUpdateApiView(generics.RetrieveUpdateDestroyAPIView):
    # permission_classes = [IsAuthenticated]

    serializer_class = ClientSerializer
    queryset = Client.objects.all()
    lookup_url_kwarg = 'id'


# Dispatch
class DispatcherListApiView(generics.ListAPIView):
    # permission_classes = [IsAuthenticated]

    serializer_class = DispatchSerializer
    queryset = Dispatch.objects.all()


class DispatcherCreateApiView(generics.CreateAPIView):
    # permission_classes = [IsAuthenticated]

    serializer_class = DispatchSerializer
    queryset = Dispatch.objects.all()


class DispatcherUpdateApiView(generics.RetrieveUpdateDestroyAPIView):
    # permission_classes = [IsAuthenticated]

    serializer_class = DispatchSerializer
    queryset = Dispatch.objects.all()
    lookup_url_kwarg = 'id'


# Message
class MessageListApiView(generics.ListAPIView):
    # permission_classes = [IsAuthenticated]

    serializer_class = MessageSerializer
    queryset = Dispatch.objects.all()


class MessageCreateApiView(generics.CreateAPIView):
    # permission_classes = [IsAuthenticated]

    serializer_class = MessageSerializer
    queryset = Dispatch.objects.all()


class MessageUpdateApiView(generics.RetrieveUpdateDestroyAPIView):
    # permission_classes = [IsAuthenticated]

    serializer_class = MessageSerializer
    queryset = Dispatch.objects.all()
    lookup_url_kwarg = 'id'
