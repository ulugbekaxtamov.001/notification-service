from django.urls import path
from api.v1.notification import views

app_name = 'api_notification'

urlpatterns = [
    # client
    path('client/list/', views.ClientListApiView.as_view(), name='client_list'),
    path('client/create/', views.ClientCreateApiView.as_view(), name='client_create'),
    path('client/<int:id>/', views.ClientUpdateApiView.as_view(), name='client_delete_update'),

    # sms
    # pass  ))

    # dispatcher
    path('dispatch/list/', views.DispatcherListApiView.as_view(), name='dispatch_list'),
    path('dispatch/create/', views.DispatcherCreateApiView.as_view(), name='dispatch_create'),
    path('dispatch/<int:id>/', views.DispatcherUpdateApiView.as_view(), name='dispatch_delete_update'),

]
