FROM python:3.10-buster
ENV TZ=UTC

WORKDIR /application

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .

USER nobody
EXPOSE 8000
